import IconHome from '../assets/icons/icons8-home.png';
import IconNilai from '../assets/icons/icons8-nilai.png';
import IconIzin from '../assets/icons/icons8-izinpulang.png';
import IconPelanggaran from '../assets/icons/icons8-pelanggaran.png';
import IconCall from '../assets/icons/icons8-call.png';

import LogoDarsalafy from '../assets/logos/logo-darsalafy.png';

import IconProgram from '../assets/icons/icons8-program.png';
import IconInfo from '../assets/icons/icons8-info.png';
import IconKami from '../assets/icons/icons8-we.png';
import IconKontak from '../assets/icons/icons8-contacts.png';

export {
  IconNilai,
  IconHome,
  IconCall,
  IconIzin,
  IconPelanggaran,
  LogoDarsalafy,
  IconProgram,
  IconInfo,
  IconKami,
  IconKontak,
};
