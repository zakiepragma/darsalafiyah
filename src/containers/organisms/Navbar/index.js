import {View} from 'react-native';
import React, {Component} from 'react';
import {
  IconCall,
  IconHome,
  IconIzin,
  IconNilai,
  IconPelanggaran,
} from '../../../assets';
import NavbarIcon from '../../../components/moleculs/NavbarIcon';

class Navbar extends Component {
  render() {
    return (
      <View
        style={{height: 54, flexDirection: 'row', backgroundColor: '#2c5f88'}}>
        <NavbarIcon icon={IconHome} judul="Home" aktif />
        <NavbarIcon icon={IconNilai} judul="Nilai" />
        <NavbarIcon icon={IconPelanggaran} judul="Pelanggaran" />
        <NavbarIcon icon={IconIzin} judul="Izin Pulang" />
        <NavbarIcon icon={IconCall} judul="Hub Admin" />
      </View>
    );
  }
}

export default Navbar;
