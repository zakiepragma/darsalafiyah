import {StyleSheet, Text, TextInput, View} from 'react-native';
import React, {useState} from 'react';
import DarsalafyLogo from '../../../components/atoms/DarsalafyLogo';
import {colors} from '../../../utils';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const Login = () => {
  const navigation = useNavigation();

  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const sendData = () => {
    console.log('data : ', form);
  };

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };
  return (
    <View style={styles.wrapper}>
      <ScrollView>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <DarsalafyLogo lebar={200} tinggi={200} />
          <Text
            style={{fontSize: 18, fontWeight: 'bold', color: colors.birukuh}}>
            Ma'had Addar Assalafiyah
          </Text>
        </View>
        <TextInput
          style={styles.input}
          placeholderTextColor={colors.birukuh}
          placeholder="Masukkan EMail"
          onChangeText={value => onInputChange(value, 'email')}
          value={form.email}
        />
        <TextInput
          style={styles.input}
          placeholderTextColor={colors.birukuh}
          placeholder="Masukkan Password"
          onChangeText={value => onInputChange(value, 'password')}
          secureTextEntry={true}
          value={form.password}
        />
        <TouchableOpacity style={styles.button} onPress={sendData}>
          <Text style={styles.textButton}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Register');
          }}>
          <Text style={styles.akun}>
            Belum memiliki akun?
            <Text style={styles.register}> Register</Text>
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderColor: colors.birukuh,
    borderRadius: 25,
    paddingVertical: 12,
    paddingHorizontal: 18,
    fontSize: 14,
    color: colors.birukuh,
    marginTop: 20,
  },
  wrapper: {
    padding: 20,
  },
  button: {
    backgroundColor: colors.birukuh,
    paddingVertical: 13,
    borderRadius: 25,
    marginTop: 30,
  },
  textButton: {
    fontSize: 12,
    color: 'white',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  akun: {
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  register: {
    color: colors.birukuh,
    fontStyle: 'italic',
  },
});

//no rek bsi : 7193941626
