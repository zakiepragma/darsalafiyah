import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import TopBar from '../../../components/moleculs/TopBar';

const Dashboard = () => {
  return (
    <View>
      <TopBar />
      <Text>Dashboard</Text>
    </View>
  );
};

export default Dashboard;

const styles = StyleSheet.create({});
