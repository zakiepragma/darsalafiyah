import {StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import DarsalafyLogo from '../../../components/atoms/DarsalafyLogo';
import {colors} from '../../../utils';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 2000);
  });
  return (
    <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
      <DarsalafyLogo lebar={200} tinggi={200} />
      <Text style={{fontSize: 18, fontWeight: 'bold', color: colors.birukuh}}>
        Ma'had Addar Assalafiyah
      </Text>
    </View>
  );
};

export default Splash;
