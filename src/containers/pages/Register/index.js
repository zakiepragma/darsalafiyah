import {Text, View} from 'react-native';
import React, {Component} from 'react';
import DarsalafyLogo from '../../../components/atoms/DarsalafyLogo';
import {colors} from '../../../utils';
class Register extends Component {
  render() {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <DarsalafyLogo lebar={200} tinggi={200} />
        <Text style={{fontSize: 18, fontWeight: 'bold', color: colors.birukuh}}>
          Ma'had Addar Assalafiyah
        </Text>
        <Text>Dalam tahap pengembangan</Text>
      </View>
    );
  }
}

export default Register;
