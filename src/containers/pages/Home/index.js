import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import TopBar from '../../../components/moleculs/TopBar';
import TopMenu from '../../../components/moleculs/TopMenu';
import FasilitasDarsalafy from '../../../components/moleculs/FasilitasDarsalafy';
import NavbarHome from '../../../components/moleculs/NavbarHome';

const Home = () => {
  return (
    <View style={styles.container}>
      <ScrollView style={{backgroundColor: 'white', flex: 1}}>
        <TopBar />
        <TopMenu />
        <FasilitasDarsalafy />
      </ScrollView>
      {/* <Navbar /> */}
      <NavbarHome />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
