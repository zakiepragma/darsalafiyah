import Home from './Home';
import Login from './Login';
import Dashboard from './Dashboard';
import Splash from './Splash';

export {Home, Login, Dashboard, Splash};
