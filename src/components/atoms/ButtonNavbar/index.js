import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

const ButtonNavbar = props => {
  return (
    <TouchableOpacity
      style={styles.opa}
      onPress={() => props.navigation.navigate(props.pindahKe)}>
      <Text style={styles.button}>{props.title}</Text>
    </TouchableOpacity>
  );
};

export default ButtonNavbar;

const styles = StyleSheet.create({
  button: {
    fontSize: 12,
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  opa: {
    backgroundColor: '#2465BD',
    borderColor: '#F9FA57',
    borderWidth: 2,
    borderRadius: 25,
    margin: 5,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
