import {Image, StyleSheet} from 'react-native';
import React from 'react';
import {LogoDarsalafy} from '../../../assets';

const DarsalafyLogo = props => {
  return (
    <Image
      source={LogoDarsalafy}
      style={{
        width: props.lebar,
        height: props.tinggi,
        marginRight: props.batasKanan ? props.batasKanan : 0,
        marginLeft: props.batasKiri ? props.batasKiri : 0,
      }}
    />
  );
};

export default DarsalafyLogo;

const styles = StyleSheet.create({});
