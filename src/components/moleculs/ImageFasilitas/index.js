import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';

const ImageFasilitas = props => {
  return (
    <View style={{marginRight: 16}}>
      <View
        style={{
          width: 150,
          height: 150,
          borderRadius: 10,
        }}>
        <Image
          source={props.gambar}
          style={{
            width: undefined,
            height: undefined,
            resizeMode: 'cover',
            flex: 1,
          }}
        />
      </View>
      <Text
        style={{
          fontSize: 16,
          fontWeight: 'bold',
          color: '#1c1c1c',
          marginTop: 12,
        }}>
        {props.nama}
      </Text>
    </View>
  );
};

export default ImageFasilitas;

const styles = StyleSheet.create({});
