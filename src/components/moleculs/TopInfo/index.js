import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

const TopInfo = () => {
  return (
    <View
      style={{
        borderLeftWidth: 3,
        borderRightWidth: 3,
        borderBottomWidth: 1,
        borderBottomColor: 'white',
        borderLeftColor: '#2c5f88',
        borderRightColor: '#2c5f88',
        backgroundColor: '#2c5f88',
      }}>
      <View style={styles.bungkusInfo}>
        <Text style={styles.textInfo}>Pendaftaran telah dibuka dari</Text>
        <Text style={styles.textTgl}>24 maret 2022 - 24 juli 2022</Text>
      </View>
      <TouchableOpacity style={styles.opa}>
        <Text style={styles.button}>Daftar</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TopInfo;

const styles = StyleSheet.create({
  button: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#2c5f88',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  opa: {
    backgroundColor: '#E6DDAA',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 10,
    marginLeft: 100,
    marginRight: 100,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInfo: {
    paddingHorizontal: 10,
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  textTgl: {
    paddingHorizontal: 10,
    color: 'white',
  },
  bungkusInfo: {
    marginBottom: 10,
    marginTop: 10,
    alignItems: 'center',
    alignContent: 'center',
  },
});
