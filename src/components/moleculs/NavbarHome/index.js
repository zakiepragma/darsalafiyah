import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {IconHome} from '../../../assets';
import ButtonNavbar from '../../atoms/ButtonNavbar';
import {useNavigation} from '@react-navigation/native';

const NavbarHome = () => {
  const navigation = useNavigation();
  return (
    <View
      style={{height: 54, flexDirection: 'row', backgroundColor: '#2c5f88'}}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
          borderWidth: 3,
          borderBottomLeftRadius: 50,
          borderBottomRightRadius: 50,
          borderBottomWidth: 3,
          borderLeftWidth: 3,
          borderRightWidth: 3,
          borderColor: '#F9FA57',
          borderTopWidth: 0,
        }}>
        <Image
          source={IconHome}
          style={{width: 26, height: 26}}
          tintColor="#2c5f88"
        />
        <Text style={{fontSize: 10, color: '#2c5f88', marginTop: 4}}>Home</Text>
      </View>
      <ButtonNavbar
        title="Register"
        pindahKe="Register"
        navigation={navigation}
      />
      <ButtonNavbar title="Login" pindahKe="Login" navigation={navigation} />
    </View>
  );
};

export default NavbarHome;

const styles = StyleSheet.create({});
