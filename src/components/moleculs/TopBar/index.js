import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';

const TopBar = () => {
  return (
    <View style={{flexDirection: 'row', padding: 5}}>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Text
          style={{
            fontSize: 25,
            fontWeight: 'bold',
            marginRight: 18,
            color: '#1A4776',
          }}>
          معهد أدار السلفية الإسلامية
        </Text>
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginRight: 8,
        }}>
        <Image
          source={require('../../../assets/logos/logo-darsalafy.png')}
          style={{
            width: 40,
            height: 40,
          }}
        />
      </View>
    </View>
  );
};

export default TopBar;

const styles = StyleSheet.create({});
