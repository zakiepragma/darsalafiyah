import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import DarsalafyLogo from '../../atoms/DarsalafyLogo';

const TopDarsalafyFeture = () => {
  return (
    <View
      style={{
        flexDirection: 'row',
        backgroundColor: '#E6DDAA',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 10,
        alignItems: 'center',
        borderTopWidth: 2,
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderBottomWidth: 1,
        borderBottomColor: 'white',
        borderTopColor: '#2c5f88',
        borderLeftColor: '#2c5f88',
        borderRightColor: '#2c5f88',
      }}>
      <DarsalafyLogo lebar={30} tinggi={30} batasKanan={15} />
      <Text style={{color: '#2c5f88', fontWeight: 'bold', fontSize: 17}}>
        DARSALAFIYAH.COM
      </Text>
    </View>
  );
};

export default TopDarsalafyFeture;

const styles = StyleSheet.create({});
