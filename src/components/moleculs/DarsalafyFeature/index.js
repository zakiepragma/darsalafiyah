import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';

const DarsalafyFeature = props => {
  return (
    <View style={{flex: 1, alignItems: 'center'}}>
      <Image style={{width: 40, height: 40}} source={props.img} />
      <Text
        style={{
          fontSize: 13,
          fontWeight: 'bold',
          color: 'black',
          marginTop: 10,
        }}>
        {props.judul}
      </Text>
    </View>
  );
};

export default DarsalafyFeature;

const styles = StyleSheet.create({});
