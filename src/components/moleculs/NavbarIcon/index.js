import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';

const NavbarIcon = props => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: props.aktif ? 'white' : '',
        borderBottomLeftRadius: props.aktif ? 50 : 0,
        borderBottomRightRadius: props.aktif ? 50 : 0,
        borderBottomWidth: props.aktif ? 3 : 0,
        borderLeftWidth: props.aktif ? 3 : 0,
        borderRightWidth: props.aktif ? 3 : 0,
        borderColor: props.aktif ? '#F9FA57' : '',
        borderTopWidth: props.aktif ? 0 : 3,
        borderTopColor: props.aktif ? '' : '#F9FA57',
      }}>
      <Image
        source={props.icon}
        style={{width: 26, height: 26}}
        tintColor={props.aktif ? '#2c5f88' : 'white'}
      />
      <Text
        style={{
          fontSize: 10,
          color: props.aktif ? '#2c5f88' : 'white',
          marginTop: 4,
        }}>
        {props.judul}
      </Text>
    </View>
  );
};

export default NavbarIcon;

const styles = StyleSheet.create({});
