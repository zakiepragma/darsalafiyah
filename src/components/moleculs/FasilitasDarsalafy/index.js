import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {LogoDarsalafy} from '../../../assets';
import ImageFasilitas from '../ImageFasilitas';

const FasilitasDarsalafy = () => {
  return (
    <View style={{marginTop: 17}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 16,
          paddingHorizontal: 16,
        }}>
        <Text style={{fontSize: 17, fontWeight: 'bold', color: '#1c1c1c'}}>
          Fasilitas Pondok
        </Text>
        <Text style={{fontSize: 17, fontWeight: 'bold', color: '#2c5f88'}}>
          View All
        </Text>
      </View>
      <ScrollView
        horizontal={true}
        style={{flexDirection: 'row', paddingLeft: 16}}>
        <ImageFasilitas gambar={LogoDarsalafy} nama="Masjid" />
        <ImageFasilitas gambar={LogoDarsalafy} nama="Perpustakaan" />
        <ImageFasilitas gambar={LogoDarsalafy} nama="Kantin" />
        <ImageFasilitas gambar={LogoDarsalafy} nama="Lapangan Bola" />
        <ImageFasilitas gambar={LogoDarsalafy} nama="Asrama" />
      </ScrollView>
    </View>
  );
};

export default FasilitasDarsalafy;

const styles = StyleSheet.create({});
