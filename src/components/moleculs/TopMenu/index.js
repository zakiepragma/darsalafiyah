import {StyleSheet, View} from 'react-native';
import React from 'react';
import TopDarsalafyFeture from '../TopDarsalafyFeture';
import {
  IconInfo,
  IconKami,
  IconKontak,
  IconNilai,
  IconProgram,
} from '../../../assets';
import DarsalafyFeature from '../DarsalafyFeature';
import TopInfo from '../TopInfo';

const TopMenu = () => {
  return (
    <View style={{marginHorizontal: 17, marginTop: 8}}>
      <TopDarsalafyFeture />
      <TopInfo />
      <View
        style={{
          flexDirection: 'row',
          paddingTop: 20,
          paddingBottom: 14,
          backgroundColor: '#F9FA57',
          borderBottomLeftRadius: 50,
          borderBottomRightRadius: 50,
          borderBottomWidth: 3,
          borderLeftWidth: 3,
          borderRightWidth: 3,
          borderBottomColor: '#2c5f88',
          borderLeftColor: '#2c5f88',
          borderRightColor: '#2c5f88',
        }}>
        <DarsalafyFeature img={IconProgram} judul="Program" />
        <DarsalafyFeature img={IconInfo} judul="Informasi" />
        <DarsalafyFeature img={IconKami} judul="Tentang Kami" />
        <DarsalafyFeature img={IconKontak} judul="Kontak" />
      </View>
    </View>
  );
};

export default TopMenu;

const styles = StyleSheet.create({});
